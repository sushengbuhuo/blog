---
title: 那些你可能不知道的微博奇技淫巧
date: 2019-06-10 19:56:06
tags:
- 奇技淫巧
- 公众号
---

微博可能是目前国内最大的公共社交平台了，今天就说说微博的一些使用技巧。

ps：有兴趣可以关注下我的微博@苏生不惑，地址 https://weibo.com/2717930601

如果你想挖我微博的黑历史，那可能要辛苦你了。毕竟我发了8万多条微博。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-0178eedbece1236a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-bc13171b78bc0d9a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 微博图床
所谓图床，就是可以保存图片，同时允许使用图片外链，现在有很多免费的图床服务，如七牛、新浪微博。还有个人开发的[http://tool.mkblog.cn/tuchuang/](http://tool.mkblog.cn/tuchuang/)
[https://www.superbed.cn/](https://www.superbed.cn/)
当然这些都不大稳定，比如极简图床就挂了[https://jiantuku.com/](https://jiantuku.com/)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-efae258e4a0ca678.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


微博作为上市公司，所以用微博作图床还是很稳定的，进入微博的微相册 ：[http://photo.weibo.com](http://photo.weibo.com/)
上传图片会生成一个微博图片地址，比如http://wx4.sinaimg.cn/mw690/c08d7e89ly1g3efjakqsaj20hs0b4dgt.jpg
![image.png](https://upload-images.jianshu.io/upload_images/17817191-d4a36eca2ba175dc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
新浪微博的图片会上传到ww1-ww4几台服务器上，同时会生成几张不同尺寸的图片，常用的有以下几种形式：
 
http://ww1.sinaimg.cn/thumbnail/c08d7e89ly1g3efjakqsaj20hs0b4dgt.jpg （缩略图）
http://ww1.sinaimg.cn/small/c08d7e89ly1g3efjakqsaj20hs0b4dgt.jpg （稍微大点的图）
http://ww1.sinaimg.cn/bmiddle/c08d7e89ly1g3efjakqsaj20hs0b4dgt.jpg （再大点的图）
http://ww1.sinaimg.cn/large/c08d7e89ly1g3efjakqsaj20hs0b4dgt.jpg（最大的）

当然你直接发微博发张图片也可以的。

为了使用方便，有人开发了微博图床 Chrome 扩展，上传图片到微博更方便了。
[https://github.com/Semibold/Weibo-Picture-Store](https://github.com/Semibold/Weibo-Picture-Store)
https://chrome.google.com/webstore/detail/pinjkilghdfhnkibhcangnpmcpdpmehk
![image.png](https://upload-images.jianshu.io/upload_images/17817191-baa6857a6c159dce.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 通过微博图片找到微博主页
一般发微博都有水印的，类似这种图片右下角有@苏生不惑，这就是我的微博昵称，这个昵称是唯一的，所以通过这个昵称可以找到我的主页。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-50c72f32072815f1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

但水印可以关闭的，比如在pc版关闭。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-a6b52f2d9f25ca22.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

而且微博昵称可以更改，如果昵称改了就找不到对应的人了。


![image.png](https://upload-images.jianshu.io/upload_images/17817191-f79f8adb1d3a0de9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
关闭水印后发的图片没有了@，上面这张图片地址是http://wx2.sinaimg.cn/large/604e48d0ly1g3b6wvy3tzj20c80ag0tq.jpg 
使用在线工具[http://www.bejson.com/othertools/pic2weibo/](http://www.bejson.com/othertools/pic2weibo/)
就可以找出微博主页，就是http://weibo.com/u/1615743184这个了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-766911b765abd003.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

其实微博地址中的链接后缀 604e48d0ly1g3b6wvy3tzj20c80ag0tq.jpg 前8位604e48d0ly是个16进制，Python用 int('604e48d0',16)转换下到10进制就是1615743184了。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-58f1be0b85e28e4b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
如果想备份你的微博相册，可以使用脚本[https://github.com/yukinotech/WeiboDL](https://github.com/yukinotech/WeiboDL)
[https://github.com/dataabc/weibospider](https://github.com/dataabc/weibospider)
https://github.com/jdeng/weof
###批量删除微博 
不想用微博了，要删除所有微博，一条条删可是个体力活，推荐使用Chrome 扩展插件档案娘助手 ，能一键删除所有微博、删除所有转发微博、删除所有包含某些关键词的微博、删除某些日期的微博、删除所有收藏的微博、或者将所有微博转为仅自己可见、取消所有点赞微博等等。[https://chrome.google.com/webstore/detail/%E6%A1%A3%E6%A1%88%E5%A8%98%E5%8A%A9%E6%89%8B/abfopppplogcojgdcfhhffngkabbhifm?utm_source=chrome-ntp-icon](https://chrome.google.com/webstore/detail/%E6%A1%A3%E6%A1%88%E5%A8%98%E5%8A%A9%E6%89%8B/abfopppplogcojgdcfhhffngkabbhifm?utm_source=chrome-ntp-icon)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-386bc4a225341b55.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
还有专业的 [http://weibo333.com/](http://weibo333.com/)
[https://www.weibo.com/ttarticle/p/show?id=2309404322284893507202](https://www.weibo.com/ttarticle/p/show?id=2309404322284893507202)
https://www.jianshu.com/p/0e34baf98fce
如果不想使用插件，也可以使用第三方脚本来删除。


如果只是删除无用的转发微博（原博已被删除/不可见 等）
```js
javascript:void((function(){var e=document.createElement('script');var ex=document.createElement('script');ex.setAttribute('type', 'application/javascript');ex.setAttribute('src','https://rawcdn.githack.com/ibesty/delete_useless_retweet_weibo/81bda850af482c844fe6e8c73f518a8e4f74fcec/bookmark.js');document.body.appendChild(e);document.body.appendChild(ex);})())

```
右键点击浏览器的书签栏 -> 添加书签 -> 把代码粘贴到网址那一栏，书签标题可以随便写.
打开微博网页版 -> 我的主页 -> 全部微博， 然后点击刚刚新建的书签，稍等即可。
 
对应的脚本还有 [https://github.com/willhunger/fuckweibo](https://github.com/willhunger/fuckweibo)
[https://github.com/Neulana/Xweibo](https://github.com/Neulana/Xweibo)
### 快捷键
输入?可以获取所有快捷键，当然这个其实并没有什么卵用，还是用鼠标方便点。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-4ea40bfd4b6f465f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-10271f43d40482ba.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 记录微博到区块链上
 发微博@BlockPin ，微博机器人 [@BlockPin](https://weibo.com/BlockPin), 会自动把 @它的消息记录到区块链上。

大致原理是，先发到 [https://demo.covenantsql.io/forum/#/](https://demo.covenantsql.io/forum/#/) 然后，最多等 1min，记录会在 CovenantSQL 链上确认。刷新页面，然后点击帖子右上角的区块浏览器链接就可以找到 SQLChain 上的记录。

 
### 正常时间序
现在的微博时间序是错乱的，所以可能会错过一些好友的最新微博动态。可以使用h5版[https://m.weibo.cn/](https://m.weibo.cn/)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-39fb4123acb413ff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
也有人开发了让PC微博首页时间线正确排布的插件。
https://greasyfork.org/zh-CN/scripts/26247
客户端可以使用国际版微博 。
### 微博热榜
想看看今天有什么热门事件，做个吃瓜群众，可以在微博搜索看到
[https://s.weibo.com/top/summary?cate=realtimehot](https://s.weibo.com/top/summary?cate=realtimehot)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-a0366305bcceaa9f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

还有第三方做的http://www.lzuntalented.cn/tool/wb.html ，每隔15分钟更新。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-ca3e071a5d3f994a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第三方做的今日热榜https://tophub.today/
![image.png](https://upload-images.jianshu.io/upload_images/17817191-35105cb7f7c01139.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-28008cf72c9970ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 特别关注
想第一时间获取关注好友的动态，微博有分组功能，将对方加入特别关注组就行了，微博会及时推送给你。

也有第三方的APP 「即刻」创建相应的微博提醒。
### 下载微博视频
看到不错微博视频想下载，可以使用[https://www.weibovideo.com/](https://www.weibovideo.com/)
比如下载这条视频
![image.png](https://upload-images.jianshu.io/upload_images/17817191-1ecd352316fe2429.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

输入微博地址 https://weibo.com/1875931727/HvMppjjgE就可以获取下载链接，直接下载到本地。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fae777840fe55d1a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fb37392494fe3079.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
还有个更好的下载网站
[https://weibomiaopai.com/](https://weibomiaopai.com/)
 ![image.png](https://upload-images.jianshu.io/upload_images/17817191-5fdfb90c64bc2e68.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

好，关于微博就这些了，如果你有更好的一些实用技巧，欢迎留言交流。

### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


