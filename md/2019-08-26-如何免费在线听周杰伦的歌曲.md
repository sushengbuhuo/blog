---
title: 如何免费在线听周杰伦的歌曲
date: 2019-08-26 15:14:12
password: 654321
tags:
- 公众号

---

我平常用网易云来听音乐 http://music.163.com/ ，网易云除了歌好听，评论更有故事。

之前已经写过网易云音乐了，有兴趣看看之前的文章[那些你可能不知道的网易云音乐奇技淫巧](https://mp.weixin.qq.com/s/LtI2piwAIDXA590NEsXvuw)

可惜周杰伦的大部分歌曲网易没有版权，还好我保存了一份杰伦所有的歌曲，如果你也想收藏的话公众号回复 `周杰伦` 获取。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-325df84a8cceef93.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

那首评论过百万的神曲《晴天》已经变成灰色。https://music.163.com/#/song?id=186016
![image.png](https://upload-images.jianshu.io/upload_images/17817191-b97b59639929a4ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-14e439051aadfd20.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这首歌的版权在QQ音乐https://y.qq.com/n/yqq/song/0039MnYb0qxYhV.html
![image.png](https://upload-images.jianshu.io/upload_images/17817191-6a67e0b6c4322613.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

而另外有些歌在虾米才有版权可以播放，所以你听一首歌的话可能需要到3个网站找，比较麻烦，这里推荐几个聚合网易，QQ，虾米，酷狗等音乐网站的工具/网站。
### listen1
Listen 1可以搜索和播放来自网易云音乐，虾米，QQ音乐，酷狗音乐，酷我音乐网站的歌曲，让你的曲库更全面。[http://listen1.github.io/listen1/](http://listen1.github.io/listen1/)

它提供了Chrome插件版，win桌面版（直接exe安装），mac桌面版，Linux桌面版，我下载的是Chrome插件版，关于插件我之前也写过一篇[Chrome 浏览器扩展神器油猴](https://mp.weixin.qq.com/s/adJFh_9LH0N-vvvYaiQqXg)

安装后使用的界面是这样的。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-659c825a87620eb7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

它集合了网易云音乐，虾米音乐，QQ音乐，酷狗音乐，酷我音乐，哔哩哔哩，咪咕音乐这几个网站的音乐资源，可以说很齐全了，比如搜索《再见二丁目》，点击就可以直接播放了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-69b49508cfe362b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 音乐聚合搜索
这种聚合搜索不限定某个网站，谷歌搜索下有很多这样的网站，如果其中某个网站挂了，换一个就是了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-22807ed2a802753e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
比如第一个[http://www.gequdaquan.net/gqss/](http://www.gequdaquan.net/gqss/)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-58b4115f91f269ba.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
直接搜索就好。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-a9bb391f048ec3bf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
搜索结果中第3个[https://music.8ziyuan.com/](https://music.8ziyuan.com/)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-c240fbb95f9c0a24.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这种网站是基于开源程序搭建的，开源地址 https://github.com/maicong/music/， 如果你会代码并有自己服务器的话也可以建一个这样的音乐聚合网站。

这里说下这个网站，可以直接免费在线听周杰伦歌曲，还能下载，不知道这个网站能存在多久，先收藏吧。
http://tool.liumingye.cn/music/?type=migu&name=%E5%91%A8%E6%9D%B0%E4%BC%A6

![image.png](https://upload-images.jianshu.io/upload_images/17817191-d399c3105f3e31ad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 音乐下载
上面的聚合网站也可以直接下载歌曲，不过也有人开发了这样的下载工具，比如Music-Downloader ，这是一款简单的音乐下载（播放）器， 功能有歌曲搜索、歌单读取、在线播放、歌曲下载（包括付费音乐），下载地址 https://github.com/messoer/Music-Downloader/releases/download/1.4.2/MusicDownloader.zip
![image.png](https://upload-images.jianshu.io/upload_images/17817191-13ba8ef4fde88e52.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-19781e3d036fafe0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
当然我更喜欢用命令行来下载，详细见之前的文章 [那些你可能不知道的网易云音乐奇技淫巧](https://mp.weixin.qq.com/s/LtI2piwAIDXA590NEsXvuw)

比如下载五月天的专辑《自传》，直接`music-get https://y.qq.com/n/yqq/album/002fRO0N4FftzY.html`
![image.png](https://upload-images.jianshu.io/upload_images/17817191-c50a453e2f5a0dae.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



### 资源
全网音乐免费下载工具https://music.sonimei.cn/ https://music.sonimei.cn/yinyue/
网易云音乐、QQ音乐一键下载工具https://github.com/winterssy/music-get
一个现代音乐播放器 https://github.com/PeterDing/chord
多站合一音乐搜索解决方案 https://github.com/maicong/music/
网易云音乐nodejs api：https://github.com/Binaryify/NeteaseCloudMusicApi         
一个可以播放“任意”音乐的播放器https://github.com/feeluown/FeelUOwn
chrome插件：声海盗，下载在线音乐插件，支持豆瓣，虾米，QQ音乐，网易云音乐等https://github.com/seekerlee/SoundPirate
如何愉快使用网易云音乐，解锁变灰歌曲！https://github.com/nondanee/UnblockNeteaseMusic
一款简单的音乐下载（播放）器 功能：歌曲搜索、歌单读取、在线播放、歌曲下载（包括付费音乐） ​​​​ https://github.com/messoer/Music-Downloader/releases https://github.com/sevenJia/music
Vue 全家桶高仿网易云音乐 mac 客户端版 https://github.com/sl1673495/vue-netease-music
qq 音乐解析网站 http://www.66it.top/

推荐阅读:

[如何发一条空白的朋友圈](https://mp.weixin.qq.com/s/Xz1m-mqtCcBF_4hmGCpkUQ)

[如何在电脑上登陆多个微信](https://mp.weixin.qq.com/s/_3AeNahwbs8c3UJ0is1t4A)

[如何提取公积金 9 天到账](https://mp.weixin.qq.com/s/qyFvOgHf1mXwPKO0tQwUyg)

[那些你可能不知道的浏览器奇技淫巧](https://mp.weixin.qq.com/s/-cSjrvkibYGp5Fx8gCTFuw)

[那些你可能不知道的微信奇技淫巧](https://mp.weixin.qq.com/s/eGDO0Y8el_dsEyriCoAgog)

[那些你可能不知道的网易云音乐奇技淫巧](https://mp.weixin.qq.com/s/LtI2piwAIDXA590NEsXvuw)

[那些你可能不知道的视频下载奇技淫巧](https://mp.weixin.qq.com/s/5InOxKmi9eXk33G_8N3byQ)

[那些你可能不知道的免费观看 VIP 视频奇技淫巧](https://mp.weixin.qq.com/s/R3x-xZwqLIVwPjlgikDQ9A)

[那些你可能不知道的知乎奇技淫巧](https://mp.weixin.qq.com/s/sqRgMh4rxFBt5YxNtaa6dw)

![免费星球](https://upload-images.jianshu.io/upload_images/17817191-393b26173c148690.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)