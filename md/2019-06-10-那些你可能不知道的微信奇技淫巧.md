---
title: 那些你可能不知道的微信奇技淫巧
date: 2019-06-10 19:57:09
tags:
- 奇技淫巧
- 公众号
---

微信是我们最常用的 APP 了，今天来说说微信的一些实用技巧。

### 验证好友是否删除你
不用群发好友或者拉群，让微信提示你对方开启了好友验证，很招人烦的。
点开对方的对话框，选择转账，输入1元，点击转账， 如果对方已经将你删除，这时会显示【你不是收款方好友】。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-a9723c1e1d32c097.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


当然如果对方拉黑你了，发消息会提示 消息已发出，但被对方拒收了。

 电脑客户端用 WeTool 就可以检测  。
### 挽回转错的钱
给微信设置到账时间。选择「支付」，点击右上角的「…」，到「支付管理」，即可设置「转账到账时间」。可以设置为「2 小时到账」或「24 小时到账」，万一你转错了帐还可以挽回。


### 朋友圈快速回到顶部
当刷了好几屏朋友圈之后，轻轻双击顶部立刻回到朋友圈顶部。
### 长截图
微信聊天记录和公众号文章都可以收藏起来以后看，为了防止他人给你发的文件失效，文件也可以收藏起来。

收藏里还有个笔记功能更实用，在【收藏】里点击右上角【+】就可以创建自己的笔记，笔记可以插入文字，图片，音频和视频，然后转发给好友或者朋友圈，这样就实现了朋友圈发语音了。

另外多张图片还有可以保存为图片，这样就实现长截图了。

而且你把笔记置顶后可以在聊天窗口顶部看到，可以当个提醒功能。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-5feaaf959d5b11fc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 快速查找错过的红包
在「查找聊天记录」，点击进入找到「交易」，就可以快速找到群里的所有红包了，挨个点吧。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-7a251a47cf88cdca.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-13a59f68f6d636bc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 给自己发消息
很多人会给 文件传输助手 这个账号发文字或者文件给用来备份。
但其实可以发给自己的微信，不用发给文件传输助手，因为之前有人把自己的头像和微信昵称改为「文件传输助手」，然后就可能收到别人发的一些文件。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-f58a32003da4c9da.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

当然也可以用面对面建群建一个人的群，在群里发。

### 引用消息并回复
群里人太多，你发的消息别人可能不知道是给谁回复的，这就需要引用了，当然这个只能在pc上使用。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fe2a70a2ce9ff224.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 搜索朋友圈
点击右上角的「搜索」，再点击「朋友圈」，输入自己的微信昵称就可以看到自己发过的朋友圈，还可以按发布时间搜索。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-c658f27ad9fbc748.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 微信提醒
在聊天窗口长按聊天内容可以设置多久以后提醒你。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-9104ae2ae5cdfef8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

当然还有个官方公众号 语音提醒 给公众号发送语音，到时间会提醒你。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-418a3bc389f8a42a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 朋友圈发花样字体
![image.png](https://upload-images.jianshu.io/upload_images/17817191-b245af04baa37e64.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
使用第三方工具 [https://beizhedenglong.github.io/weird-fonts/](https://beizhedenglong.github.io/weird-fonts/)

![beizhedenglong.github.io_weird-fonts_.png](https://upload-images.jianshu.io/upload_images/17817191-0365cef06a3c779b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 找到自己所有的微信群 

保存到通讯录的群可以在发起群聊里找到，但如果没有保存到通讯录，也没有人在群里发消息怎么能找到那个群呢，打开搜索，输入自己的微信昵称，点击更多群聊就可以找到自己曾经加过的的所有群啦！
![image.png](https://upload-images.jianshu.io/upload_images/17817191-2cbfbfbe8b6d0409.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### pc上查看公众号文章
微信官方的搜狗微信 [https://weixin.sogou.com/](https://weixin.sogou.com/)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fdbced5b8df751b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

二十次幂[https://www.ershicimi.com/](https://www.ershicimi.com/)
瓦斯阅读[https://qnmlgb.tech/](https://qnmlgb.tech/)
vreadtech[https://www.vreadtech.com/](https://www.vreadtech.com/)
传送门https://chuansongme.com/

### 微信好友图片墙
将所有微信好友的头像拼成一张图，具体可以查看之前的文章https://mp.weixin.qq.com/s/GK3fp-cUSeByUrqINGFXHg 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-63cd7ed602558e95.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

还有第三方开发的微信群合影插件 [http://www.qunheying.com/](http://www.qunheying.com/)

### 导出公众号
免费公众号采集工具WCplus ，具体使用查看文章 [https://shimo.im/docs/dA7ejdOQuPwo7NZV/read](https://shimo.im/docs/dA7ejdOQuPwo7NZV/read)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fb9d58521b820ea8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
吾爱破解论坛牛人的公众号下载工具 [https://www.52pojie.cn/forum.php?mod=viewthread&tid=641714&page=1](https://www.52pojie.cn/forum.php?mod=viewthread&tid=641714&page=1)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-257e0b7aa1629016.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
下载该工具需要注册论坛，如果你不方便的话可以在公众号 苏生不惑 发送 公众号下载  获取下载链接 。

当然还有第三方开发的基于搜狗微信搜索的微信公众号爬虫 [https://github.com/axiref/WechatSogou](https://github.com/axiref/WechatSogou)
[https://github.com/songluyi/crawl_wechat](https://github.com/songluyi/crawl_wechat)
[https://github.com/leo8916/wxhub](https://github.com/leo8916/wxhub)
### 微信防撤回
使用微信小助手，不过不建议使用。

### 导出朋友圈
这个需要第三方工具，之前找好友把我的朋友圈都导出来，我把导出的朋友圈放在网页上了，有兴趣可以在我公众号看到。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-65a22e5d9e6fa362.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

如果你也想导出朋友圈可以联系我，当然不能保证一定能成功，因为微信可能有调整策略。

 

当然还有将你的朋友圈出书，比如微信书 [https://weixinshu.com/](https://weixinshu.com/)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-6eabc0ab348cc7ff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

最后如果你想了解更多关于微信的最新资讯可以关注官方公众号 微信派 和腾讯。

还有哪些不为人知的微信实用技巧，欢迎留言评论。



推荐阅读：
[那些你可能不知道的浏览器奇技淫巧](http://mp.weixin.qq.com/s?__biz=MzIyMjg2ODExMA==&mid=2247483950&idx=1&sn=10a98508465904c89797e596b7b3ee70&chksm=e827a5cfdf502cd9340bfe83bb434fefc2a8f5e38a2a48a2f6b038bb7c93d8fc022cf070d6c2&scene=21#wechat_redirect)

### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


