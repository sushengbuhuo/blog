---
title: 微博十年了，你还玩吗?
date: 2019-09-03 11:16:43
tags:
- 公众号
---
微博09年诞生，一晃10年过去了，记得我第一条微博是2012年4月23 https://weibo.com/2717930601/yfTTlFGr8 

![image](https://upload-images.jianshu.io/upload_images/17817191-4fffe89bcaf991d9?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-25b818577c4d062f?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

7年时间微博发了8万多条，我也是佩服自己。

![image](https://upload-images.jianshu.io/upload_images/17817191-369c42d228a6c163?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这周（8月28号）微博举行十周年活动，微博小秘书创建了微博十年的话题。https://weibo.com/aj/static/tenyears.html

![image](https://upload-images.jianshu.io/upload_images/17817191-4bd5e7cd141377e5?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

微博转发300多万。

![image](https://upload-images.jianshu.io/upload_images/17817191-ead6d2bc5c947b5d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

玩微博这么久，挺后悔的一件事就是没有好好运营，没有成为某方面的大v，粉丝目前还没破万。

![image](https://upload-images.jianshu.io/upload_images/17817191-83a1f862f4bf24bb?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

下面是我在公司拍的一些照片。

![image](https://upload-images.jianshu.io/upload_images/17817191-9ba91d0d1b662cd3?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-2eef766525e98b2b?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-0f15d477ebfec028?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-80f70891605bd30d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 ![image](https://upload-images.jianshu.io/upload_images/17817191-2ea133afd9197aa3?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-46cc8ab128284104?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-3e9c8dca1b4c1c57?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-7c32c3bf6a925e8d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-5f889cac54052a12?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 还拍了个微博视频，地址https://weibo.com/2717930601/I4kyJE8E4 

https://weibo.com/1642909335/I4gCrzR72 

还上了个假的热搜。

![image](https://upload-images.jianshu.io/upload_images/17817191-dadc1b54efc7ff4e?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-72bd26c9b40a78ec?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**媒体总结的近十年的35部爆款剧，你看过多少？**

![image](https://upload-images.jianshu.io/upload_images/17817191-724edeec66dcf67d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

下面的图片来自微博 

https://weibo.com/6046360157/I4ilE6GL3 

https://weibo.com/3031969212/I4iFCwLMM

https://weibo.com/1385892075/I4jsF6AYU

![image](https://upload-images.jianshu.io/upload_images/17817191-376f8c95b07af5bf?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-e6dbfda915f61edd?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-19572ec22fb1a2fe?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-be99a00b4769614e?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-c9079a647da4a65b?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-635cfa89c8b91d93?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-5814306eaa57d2f1?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 苹果我还没领。。。

![image](https://upload-images.jianshu.io/upload_images/17817191-65113eeee15cbac9?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-eacb9fe49a0d7ede?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-2b82ac0dcbf182da?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

最后祝福微博，下个十年更精彩。
