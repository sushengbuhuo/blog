---
title: 怕被发垃圾邮件？用临时邮箱来注册账号
date: 2019-09-16 20:25:44
tags:
- 公众号
---

注册网站一般都需要邮箱，但有时候不想用自己的邮箱来注册，有些网站总给你发垃圾广告，这时可以用临时邮箱来注册。

临时邮箱也被称为一次性邮箱和匿名邮箱，就是提供一个简易的临时邮箱服务，不需要注册，打开网页即可使用，但临时邮箱时效性很短，通常只有10分钟，一小时或者一天，所以不能作为一个长期固定的邮箱来使用。

### 十分钟邮箱
https://bccto.me/  生成一个自定义邮箱地址 susheng@bccto.me  
![image.png](https://upload-images.jianshu.io/upload_images/17817191-dee47cedf254f8bc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
很快收到一封邮件，可以查看到邮件内容
![image.png](https://upload-images.jianshu.io/upload_images/17817191-87759b7c6747e3d3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 三十分钟邮箱
[https://shorttimemail.com/zh-Hans](https://shorttimemail.com/zh-Hans) 生成一个临时邮箱 k8sf27e_j@shorttimemail.com 

![image.png](https://upload-images.jianshu.io/upload_images/17817191-564899a2f1f214a5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-05590edc1b5bb541.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 一小时邮箱
[https://www.moakt.com/zh](https://www.moakt.com/zh) 生成一个自定义邮箱地址susheng@disbox.net 有效期为一个小时，自己发送邮箱测试下效果。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-62fa5038c3c6a98c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-4d0a7e767c93836b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

过期前还能延时
![image.png](https://upload-images.jianshu.io/upload_images/17817191-3d3772ab3812a98c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 24小时邮箱
[http://24mail.chacuo.net/](http://24mail.chacuo.net/) 这个网站会临时给你生成个邮箱地址，你也可以自定义，比如这个 zjupef35610@chacuo.net ，24小时后失效，网站给这个邮箱发的邮件直接可以在下面显示出来。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-604617076789725c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-29de33c926ec87b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

如果网站还需要手机号注册，也怕被发垃圾短信，同样有临时手机号来接受短信，比如https://www.pdflibr.com/SMSContent/1 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-20f64a1830bdf1cb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


### 其他 
[https://temp-mail.org/zh/](https://temp-mail.org/zh/)
https://9em.org/
短信http://www.z-sms.com/ 
高效搜罗导航http://www.gaoxiaosouluo.cn/
 代接受验证码的网站，
http://www.smszk.com/ 如果不想用自己的手机接受验证码的话，可以用这个可以暂时解决短信收发的问题
代收短信 www.pdflibr.com/
https://sms.bilulanlv.com/message/3322077483.html 
在线收到短信 https://www.becmd.com/
 https://www.cnwml.com/
免费ssr https://www.attackmen.com/ 

推荐阅读:

[如何发一条空白的朋友圈](https://mp.weixin.qq.com/s/Xz1m-mqtCcBF_4hmGCpkUQ)

[免费在线听周杰伦歌曲](https://mp.weixin.qq.com/s/1omFkK5PPyeJEzUTagj9qg)

[那些你可能不知道的微信奇技淫巧](https://mp.weixin.qq.com/s/eGDO0Y8el_dsEyriCoAgog)

[如何在豆瓣租房小组快速找到满意的房子](https://mp.weixin.qq.com/s/k5lBwiDzGgSU3fh2v2Rw9A)

[10%+10% 不等于 0.2？](https://mp.weixin.qq.com/s/qNfuWjH54WHJtx4sEE5xwA)

[Chrome 浏览器扩展神器油猴](https://mp.weixin.qq.com/s/adJFh_9LH0N-vvvYaiQqXg)


![免费星球](https://upload-images.jianshu.io/upload_images/17817191-393b26173c148690.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


