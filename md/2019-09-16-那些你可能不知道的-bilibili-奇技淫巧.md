---
title: 那些你可能不知道的 bilibili 奇技淫巧
date: 2019-09-16 20:28:12
tags:
- 公众号
---
这是奇技淫巧系列第9篇文章了。

[那些你可能不知道的浏览器奇技淫巧]( https://mp.weixin.qq.com/s/-cSjrvkibYGp5Fx8gCTFuw)

[那些你可能不知道的知乎奇技淫巧](https://mp.weixin.qq.com/s/sqRgMh4rxFBt5YxNtaa6dw)

[那些你可能不知道的微信奇技淫巧](https://mp.weixin.qq.com/s/eGDO0Y8el_dsEyriCoAgog)

[那些你可能不知道的微博奇技淫巧](https://mp.weixin.qq.com/s/j7VhoZXmUTnOWC5C_B8jlQ)

[那些你可能不知道的网易云音乐奇技淫巧]( https://mp.weixin.qq.com/s/LtI2piwAIDXA590NEsXvuw)

[那些你可能不知道的搜索奇技淫巧](https://mp.weixin.qq.com/s/-5tZWfeWWa_E8jRCH0T_Cw)

[那些你可能不知道的视频下载奇技淫巧](https://mp.weixin.qq.com/s/5InOxKmi9eXk33G_8N3byQ)

[那些你可能不知道的免费观看 VIP 视频奇技淫巧](https://mp.weixin.qq.com/s/R3x-xZwqLIVwPjlgikDQ9A)

b站在国内视频网站是一股清流，一个看视频不用等60s广告的网站，今天就说说这个二次元网站的一些实用工具/技巧。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-734c02d1fb334e94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 哔哩哔哩助手
https://github.com/bilibili-helper/bilibili-helper  这是一个b站 辅助工具 https://bilibili-helper.github.io/ ，可以替换播放器、去广告、推送通知并进行一些快捷操作。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-9d41b316c481ade0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

Chrome扩展下载链接 [https://chrome.google.com/webstore/detail/%E5%93%94%E5%93%A9%E5%93%94%E5%93%A9%E5%8A%A9%E6%89%8B%EF%BC%9Abilibilicom-%E7%BB%BC%E5%90%88%E8%BE%85%E5%8A%A9%E6%89%A9%E5%B1%95/kpbnombpnpcffllnianjibmpadjolanh](https://chrome.google.com/webstore/detail/%E5%93%94%E5%93%A9%E5%93%94%E5%93%A9%E5%8A%A9%E6%89%8B%EF%BC%9Abilibilicom-%E7%BB%BC%E5%90%88%E8%BE%85%E5%8A%A9%E6%89%A9%E5%B1%95/kpbnombpnpcffllnianjibmpadjolanh)


比如这个视频 https://www.bilibili.com/video/av10914119 ，安装扩展登录b站后右侧有个助手按钮，可以直接下载当前播放的视频和弹幕。  
![image.png](https://upload-images.jianshu.io/upload_images/17817191-5224a2893e01864b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

更多功能可以自己设置
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fd158e3deba3e186.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 观看港澳台番剧 
有些番剧限制了港澳台才能看，比如 https://www.bilibili.com/bangumi/play/ss28049 这个会提示无法观看。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-f724ce76cb2b423e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这时候就需要使用油猴了，之前写过怎么安装油猴 [Chrome 浏览器扩展神器油猴](https://mp.weixin.qq.com/s/adJFh_9LH0N-vvvYaiQqXg)

 安装这个脚本 https://greasyfork.org/zh-CN/scripts/25718 ，有问题查看这里 https://github.com/ipcjs/bilibili-helper/blob/user.js/bilibili_bangumi_area_limit_hack.md 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-aa112a8acd69bc1a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


 
安装脚本后就可以愉快的看这个视频了，页面上有些文字也成繁体了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-c6319b994ddf2110.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

ps:如果你是用的Windowns 10 ，可以在window应用市场里下载UWP版本的bilibili，就可以直接看了。
### 视频下载
可以直接在https://www.ibilibili.com/ 输入播放地址，或者直接改为 https://www.ibilibili.com/video/av10914119 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-a18c42aa8088286f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

对应的还有 https://www.jijidown.com/video/av10914119
![image.png](https://upload-images.jianshu.io/upload_images/17817191-72a34be095d56285.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

还有人开发了客户端版本下载工具，公众号回复 `b站`获取。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-f46337e0be7de8d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


当然我更推荐使用命令行来下载[那些你可能不知道的视频下载奇技淫巧](https://mp.weixin.qq.com/s/5InOxKmi9eXk33G_8N3byQ)
```js
 you-get -i https://www.bilibili.com/video/av36043614
site:                Bilibili
title:               一个MV 周杰伦《晴天》
streams:             # Available quality and codecs
    [ DASH ] ____________________________________
    - format:        dash-flv720
      container:     mp4
      quality:       高清 720P
      size:          80.8 MiB (84735780 bytes)
    # download-with: you-get --format=dash-flv720 [URL]

    - format:        dash-flv480
      container:     mp4
      quality:       清晰 480P
      size:          42.4 MiB (44437476 bytes)
    # download-with: you-get --format=dash-flv480 [URL]

    - format:        dash-flv360
      container:     mp4
      quality:       流畅 360P
      size:          16.3 MiB (17131306 bytes)
    # download-with: you-get --format=dash-flv360 [URL]

    [ DEFAULT ] _________________________________
    - format:        flv720
      container:     flv
      quality:       高清 720P
      size:          81.0 MiB (84952120 bytes)
    # download-with: you-get --format=flv720 [URL]

    - format:        flv480
      container:     flv
      quality:       清晰 480P
      size:          42.6 MiB (44653816 bytes)
    # download-with: you-get --format=flv480 [URL]

    - format:        flv360
      container:     flv
      quality:       流畅 360P
      size:          16.5 MiB (17347646 bytes)
    # download-with: you-get --format=flv360 [URL]
```
### B站封面提取
每个视频都有封面图片，可以在 http://www.galmoe.com/ 输入 id号获取，比如 https://www.bilibili.com/video/av10914119 这个视频，输入 av10914119  获取到封面https://i2.hdslb.com/bfs/archive/6ca1b6647b4b97221ef82fec7148eee4920db8fe.png 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-5425a1f8e7af8d9d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-2e10710fd0835bec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

除了网站还有对应的Chrome扩展 https://github.com/jsososo/bilibili-hand-in-the-cover
### TOP 100 UP 主
http://rank.python666.cn/ 这里可以看到UP 主粉丝 TOP 100 榜单，papi酱 我偶尔也看看。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-acc896ef999f6191.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### kanbilibili
这是一个基于b站的第三方网站，bilibili 网址前面加上 kan https://www.kanbilibili.com/  ，会定期发布 日报、周刊、月刊、bilibili总榜、Up主排行榜 等数据统计报告，视频详情页提供 历史数据查看、B站视频下载、B站视频封面下载 等功能。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-02e73bf57b2fa7ad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

比如上面的视频在这里的地址就是https://www.kanbilibili.com/video/av10914119
![image.png](https://upload-images.jianshu.io/upload_images/17817191-d209b214dbb0cb88.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


可以看到UP主排行榜，每周五更新  https://www.kanbilibili.com/rank/ups/fans ，我关注的papi酱有537万粉丝，播放数有3个多亿。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-20c8e617fe3b74d7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

还有视频转mp3功能，不过还是建议使用 ffmpeg 在命令行转换，可以看我之前的文章[ffmpeg 将视频转GIF](https://mp.weixin.qq.com/s/624Hv1krGUboecz1QX8O1g)
### 粉丝在线统计
比如输入papi酱 [https://space.bilibili.com/1532165/](https://space.bilibili.com/1532165/) 的ID 1532165 可以看到具体的粉丝数，而不是b站上的538万。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-9e9442e75f600a5a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-ad6adabe77a3fe2e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### b站观测者
https://www.biliob.com 这个网站的目的是搜集并观测B站的UP主、视频或番剧等数据。 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-acc0e717aec72ca9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 直播录制工具
B站录播姬直接下载直播流，经过处理后保存到硬盘。支持自动根据文件大小或录制时长切割视频文件，支持只保存即时回放剪辑https://rec.danmuji.org/

![image.png](https://upload-images.jianshu.io/upload_images/17817191-9f9fa0c5d8977f75.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

对应的还有直播弹幕工具 https://www.danmuji.org/
![image.png](https://upload-images.jianshu.io/upload_images/17817191-9ca30e82ca954bff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 增强脚本
强大的哔哩哔哩增强脚本: 下载视频, 音乐, 封面, 弹幕 / 自定义播放器画质, 模式, 布局 / 自定义顶栏, 删除广告, 夜间模式 / 触屏设备支持https://github.com/the1812/Bilibili-Evolved，需要浏览器安装Tampermonkey插件。

各种视频相关功能有： 
![image.png](https://upload-images.jianshu.io/upload_images/17817191-4bfc9707d0b480e7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-27d745f8a7745def.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### bilibili视频编辑器
bilibili视频编辑器是一款线上视频编辑工具，无需下载，直接在网页浏览器中进行操作 ，具体使用见[https://www.bilibili.com/blackboard/help.html#bilibili%E8%A7%86%E9%A2%91%E7%BC%96%E8%BE%91%E5%99%A8?id=c4b1536079b04e5cbeb5a35606ea5137](https://www.bilibili.com/blackboard/help.html#bilibili%E8%A7%86%E9%A2%91%E7%BC%96%E8%BE%91%E5%99%A8?id=c4b1536079b04e5cbeb5a35606ea5137)
下载地址 [https://bilibili.clipchamp.com/](https://bilibili.clipchamp.com/)，需要使用b站账号登录 ，只支持Chrome浏览器。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-e5bf0fee5d4cd234.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-ca8ca972f1acd552.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

`如果文章对你有帮助，还望不吝点个在看支持下我写下去的动力。`

### 其他
小丸工具箱[https://maruko.appinn.me/](https://maruko.appinn.me/)
视频下载https://toutiao.iiilab.com/ 
油猴脚本 解析各大视频网站VIP视频[https://github.com/syhyz1990/media/](https://github.com/syhyz1990/media/)
https://www.biliplus.com/
 [如何解除B站](https://go2think.com/bilibili-nolimit/)
观看港澳台番剧 https://zhaoolee.com/ChromeAppHeroes/page/016_jie_chu_b_zhan_qu_yu_xian_zhi.html
助手https://zhaoolee.com/ChromeAppHeroes/page/021_bi_li_bi_li_zhu_shou.html 
bilibili 视频下载 油猴插件https://github.com/evgo2017/bilibili_video_download 
强大的哔哩哔哩增强脚本https://github.com/the1812/Bilibili-Evolved  
https://b23.tv/av58983681
B站2000万用户分析https://zhuanlan.zhihu.com/p/24434456  
Bilibili 用户爬虫https://github.com/airingursb/bilibili-user http://ursb.me/bilibili-report/  
抓取了BiliBili能获取哪些数据？https://www.jianshu.com/p/1c263f39e68a 
Bilibili 数据接口整理https://juejin.im/entry/58636c58128fe1006bc5588b
Python爬取bilibili全站用户信息https://juejin.im/post/5b8fe524e51d450e531c3dcd
b站视频数据播报 https://sharecuts.cn/shortcut/1068 
B站视频数据分析报告http://z_plus.coding.me/
b站指数http://www.gsdata.cn/rank/morerank?v_type=bilibili 
https://wuyin.io/2018/03/31/bilibili-live-crawler-and-auto-edit-recording/ 
如何免费采集哔哩哔哩视频数据http://www.houyicaiji.com/?type=post&pid=3812 
https://github.com/AInoob/bzhua 
http://www.bilijk.tk/ 
 A站B站数据分析实战http://www.woshipm.com/data-analysis/1622826.html
bilibili哔哩哔哩下载助手https://chrome.google.com/webstore/detail/bilibili%E5%93%94%E5%93%A9%E5%93%94%E5%93%A9%E4%B8%8B%E8%BD%BD%E5%8A%A9%E6%89%8B/bfcbfobhcjbkilcbehlnlchiinokiijp?hl=zh-CN  
B站下载助手https://docs.qq.com/doc/DQ2lhaWRpS0tubVVF
  https://esu.wiki/Bilibili   
哔哩哔哩动画第三方客户端https://github.com/DaweiX/bilibili
第三方哔哩哔哩API提供平台https://9bl.bakayun.cn/  
一个可以观看斗鱼/虎牙/熊猫/B站/全民等直播平台的第三方客户端https://www.moerats.com/archives/531/
B 站自动领瓜子、直播挂机脚本 https://github.com/metowolf/BilibiliHelper
https://docs.rsshub.app/anime.html#_005-tv
扒视频、视频合并、视频转码
 [https://media.weibo.cn/article?object_id=1022%3A2309404296500149639994&extparam=lmid--4296500152985262&luicode=10000011&lfid=1076033928669301&id=2309404296500149639994&sudaref=www.google.com.hk&display=0&retcode=6102](https://media.weibo.cn/article?object_id=1022%3A2309404296500149639994&extparam=lmid--4296500152985262&luicode=10000011&lfid=1076033928669301&id=2309404296500149639994&sudaref=www.google.com.hk&display=0&retcode=6102)
  如何在YouTube和bilibili(哔哩哔哩B站)上评论图片
https://sphard.com/%E5%A6%82%E4%BD%95%E5%9C%A8YouTube%E5%92%8Cbilibili(%E5%93%94%E5%93%A9%E5%93%94%E5%93%A9B%E7%AB%99)%E4%B8%8A%E8%AF%84%E8%AE%BA%E5%9B%BE%E7%89%87%EF%BC%8C%E5%9C%A8%E8%AF%84%E8%AE%BA%E9%87%8C%E6%8F%92%E5%85%A5%E5%9B%BE%E7%89%87.html
B站视频下载   [https://github.com/evgo2017/bilibili_video_download](https://github.com/evgo2017/bilibili_video_download)   
B 站排行https://netobs.top/  
B 站用户数据收集 https://github.com/cwjokaka/bilibili_member_crawler
B站最近推出了一个在线视频编辑工具，试了下，意外好用！最高可输出1080P！但目前仅支持Chrome浏览器。https://bilibili.clipchamp.com/editor
b 站的倍速油猴脚本，可自定义播放速度，刷新不丢失 https://greasyfork.org/en/scripts/388225
用python爬虫追踪知乎/B站大V排行 https://mp.weixin.qq.com/s/unxNMlG-TNMRbG2W1ws6sw
[youtube播放视频倍速自定义](https://greasyfork.org/en/scripts/381176)
b 站视频封面获取 https://github.com/jsososo/bilibili-hand-in-the-cover
自动搬运B博动态至微博的机器人https://github.com/smilecc/weibo-porter

推荐阅读:

[如何发一条空白的朋友圈](https://mp.weixin.qq.com/s/Xz1m-mqtCcBF_4hmGCpkUQ)

[免费在线听周杰伦歌曲](https://mp.weixin.qq.com/s/1omFkK5PPyeJEzUTagj9qg)

[想看的公众号文章被删了怎么办？](https://mp.weixin.qq.com/s/l2bQJk1qjb6IzroODBpoOg)

[如何在豆瓣租房小组快速找到满意的房子](https://mp.weixin.qq.com/s/k5lBwiDzGgSU3fh2v2Rw9A)

[10%+10% 不等于 0.2？](https://mp.weixin.qq.com/s/qNfuWjH54WHJtx4sEE5xwA)

[有意思的举牌告白小人](https://mp.weixin.qq.com/s/hbzmqep2JMICL6WogE9TCQ)


### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)