---
title: 微信小程序项目收集
date: 2019-02-25 15:16:04
tags:
- 微信
- 小程序
---
### 小程序登录流程

![i](https://iocaffcdn.phphub.org/uploads/images/201903/03/26968/uiTFdowDpC.jpg!large)

### 微信小程序版博客
```javascript
软件架构 https://learnku.com/articles/24562#topnav

开发工具：微信开发者工具
语言：直接使用原生的
项目地址：https://github.com/yanthink/mpblog

小程序后端

开发工具：PhpStorm
语言：PHP
框架：Laravel 5.6
数据库：Mysql 8.0
队列：Redis
全文搜索：Elasticsearch
websocket：Redis + websockets/ws
项目地址：https://github.com/yanthink/blog-api

后台管理

开发工具：WebStorm
语言：JavaScript
框架：ant design pro 2.0.0-beta.1
项目地址：https://github.com/yanthink/blog

运维

反向代理：Nginx 1.12.2
SSL证书：Symantec 免费版
服务器：阿里云轻量级服务器 CentOS 7.3
```
### 知识付费小程序
```javascript
https://github.com/SmallRuralDog/yundocs
yarn global add @tarojs/cli
git clone https://github.com/SmallRuralDog/yundocs.git yundocs
cd yundocs
yarn install
yarn run dev:weapp
```
### 配置小程序前后端
```javascript
git clone https://github.com/jc91715/xcx-sina-sae.git project
cd project && npm install
npm run --watch

https://learnku.com/articles/23948
```
### 小程序社区
```javascript
http://yike.io/
前端：https://github.com/overtrue/yike.io
后端：https://github.com/overtrue/api.yike.io
视频：https://learnku.com/courses/laravel-package/yikeio/2505
```
[整理小程序登录状态维护笔记](https://learnku.com/articles/28072#topnav)

[微信小程序开发问题集锦](https://blog.mutoe.com/2019/button-border-in-wechat-miniprogram/)
藏头诗微信小程序，表白，祝福，整蛊。还可以抖音去水印。
小程序码，微信直接扫一扫打开，或者微信搜：快抖工具箱
百度AI体验中心微信小程序
[小程序图片裁剪插件](https://github.com/foolstack-omg/wepy-image-cropper)
