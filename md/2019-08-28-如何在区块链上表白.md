---
title: 如何在区块链上表白
date: 2019-08-28 20:10:06
tags:
- 公众号
---
关于区块链，今年已经大火了，就不多做说明了，怎么将一段文字留永久在区块链上呢，比如给女朋友表白，代表天长地久。

首先你创建一个以太坊钱包，使用 imtoken 就好，注册 一个钱包地址，比如我的地址是 0x2DE12E54A4B5d43210e0BDd90F305E3f3a9f0403  需要备份好私钥,同时你需要先转入一点eth到钱包，因为转账的时候需要旷工费，大概0.04eth。 

然后找一个将文字转化为16进制的工具，比如这个 https://brainwalletx.github.io/#converter  如果你要储存图片，先图片转化成base64编码，再把编码转成16进制码。

xxx，我爱你，我需要你！ 这段文字转换为16进制就是 0x787878efbc8ce68891e788b1e4bda0efbc8ce68891e99c80e8a681e4bda0efbc81

![image](https://upload-images.jianshu.io/upload_images/17817191-7ba9c19751d37df7?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

之后 打开软件 Imtoken，进入转账页面，打开高级选项。将上面的十六进制粘贴进去。gas price 建议填20，gas 建议填200000。

![image](https://upload-images.jianshu.io/upload_images/17817191-be12d78f1eaa40bb?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-6139a4d9eefc3cde?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

转账成功大概需要等待10分钟，成功后复制URL 就是这个地址了 https://etherscan.io/tx/0xbe56253cfdbe9d01b46c937ccb0e8b07f4d6c0e1ebf9bd402982b3057ef89b13 点击阅读原文查看

![image](https://upload-images.jianshu.io/upload_images/17817191-0dadacce988329ab?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image](https://upload-images.jianshu.io/upload_images/17817191-57ffcb2508905403?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

点击 conver to ut8 就显示中文了，只要以太坊还存在，你的表白就被上万个以太坊矿工见证，永远写入区块链，载入史册，永不可篡改，而这一切只花费了5块钱，当然前提是你有女朋友。。。

![image](https://upload-images.jianshu.io/upload_images/17817191-45277706e7754b76?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
