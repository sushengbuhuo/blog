---
title: leetcode 题解
date: 2019-03-19 15:15:39
tags:
- leetcode
---
### Two Sum
```javascript
给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那两个整数，并返回他们的数组下标。
给定 nums = [2, 7, 11, 15], target = 9
因为 nums[0] + nums[1] = 2 + 7 = 9
所以返回 [0, 1]
function twoSum($nums, $target)
    {
        if (!isset($nums[1])) return;
        foreach ($nums as $key => $num) {
            unset($nums[$key]);
            $result = array_search($target - $num, $nums);
            if ($result !== false) return [$key, $result];
        }
    }

class Solution {

    /**
     * @param String $J
     * @param String $S
     * @return Integer  https://learnku.com/articles/27545
     */
    function numJewelsInStones($j, $s) {

        $jArr = str_split($j);
        $sArr = str_split($s);
        $i = 0;
        foreach($sArr as $sItem){
            if(in_array($sItem, $jArr)){
                $i++;
            }
        }
        return $i;

    }
}
```
### 在常数时间内检索到最小元素的栈
```javascript
class MinStack
{

    protected $stack;

    /**
     * initialize your data structure here.
     */
    function __construct()
    {
        $this->stack = [];
    }

    /**
     * @param Integer $x
     * @return NULL
     */
    function push($x)
    {
        $this->stack[] = $x;
    }

    /**
     * @return NULL
     */
    function pop()
    {
        array_pop($this->stack);
    }

    /**
     * @return Integer
     */
    function top()
    {
        return end($this->stack);
    }

    /**
     * @return Integer
     */
    function getMin()
    {
        return min($this->stack);
    }
}
```
### Reverse Integer
```javascript
输入: 123
输出: 321
function reverse($x) {
        $result = 0;
        while (abs($x) > 9) {
            $num = $x % 10;
            $x = (int)($x / 10);
            $result = $result * 10 + $num;
        }
        $result = $result * 10 + $x;
        if (strlen(base_convert($result, 10, 2)) > 31) return 0;
        return $result;
    }
```
### 回文数
```javascript
输入: 121
输出: true

输入: -123
输出: false
 function isPalindrome($x)
    {
        if ($x < 0) return false;
        if ($x % 10 === 0 && $x != 0) return false;
        $reverse = 0;
        while ($x > $reverse) {
            $reverse = (int)($reverse * 10 + $x % 10);
            $x = (int)($x / 10);
        }
        return $x === $reverse || $x === (int)($reverse / 10);
    }
```
### Remove Linked List Elements
```javascript
/**
     * @param ListNode $head
     * @param Integer $val
     * @return ListNode
     */
    function removeElements($head, $val) {
        if(!$head){
            return null;
        }
        $data=$head;
        while($data->next){
            if($data->next->val==$val){
                $data->next=$data->next->next;
            }else{
                $data=$data->next;
            }
        }
        return $head->val==$val?$head->next:$head;      
  }
}
```
[CTF 相关的内容](https://lbjheiheihei.xyz/2018/08/23/CTF-2.html)

[ctf 挑战](https://cgctf.nuptsast.com/challenges#Web)

[LeetCode 算法题 1. Two Sum](http://cs-cjl.com/2016/06_23_leetcode_1_two_sum)

[Leetcode基础刷题之PHP解析(112. Path Sum)](https://laravelacademy.org/post/19641.html)

[尝试 Leetcode https://github.com/icecho/leetcode](https://learnku.com/articles/25478)

[Leetcode之PHP版题目解析](https://laravelacademy.org/tags/leetcode)

[leetcode中国](https://leetcode-cn.com/)

[科学上网](https://github.com/haoel/haoel.github.io)

[挑战用 PHP 在 LeetCode 刷算法题](https://learnku.com/articles/25461#topnav)

[ARTS 挑战](http://blessjing.cn/index.php/2019/03/31/arts-%E6%8C%91%E6%88%98%EF%BC%88%E7%AC%AC%E4%BA%8C%E5%91%A8%EF%BC%89/)

[LEETCODE 编程训练](https://github.com/haoel/leetcode)

[PHP 开始 leetcode 刷题时光](https://github.com/wuqinqiang/leetcode-php)

[Leetcode 二叉树题目集合](https://learnku.com/articles/29219)

[Leetcode 全套题解](https://github.com/8090Lambert/Leetcode-Example)