---
title: linux 笔记
date: 2019-04-26 16:54:15
tags:
- linux
---
### 磁盘空间不足
```javascript
df命令查看当前计算器磁盘空闲情况
 df -a
 从根目录下开始使用du命令查找出空间占用最大的文件
  du -sh /*命令一路追查
 [root@test-os testuser]# df -h   ###物理磁盘空间 查看所有block使用情况
 Filesystem      Size  Used Avail Use% Mounted on
 /dev/sda3       8.8G  8.8G     0 100% /
 tmpfs           931M     0  931M   0% /dev/shm
 /dev/sda1       190M   40M  141M  22% /boot
 [root@test-os testuser]# du -sh /usr/* |grep G  ###查找大文件
 7.3G    /usr/local
 [root@test-os testuser]# du -sh /usr/local/* |grep G
 7.3G    /usr/local/bin
 [root@test-os testuser]# du -sh /usr/local/bin/* |grep G
 7.3G    /usr/local/bin/1g
 
 [root@test-os testuser]# rm -f /usr/local/bin/1g  ###删除大文件
有些文件删除时还被其它进程占用，此时文件并未真正删除，只是标记为 deleted，只有进程结束后才会将文件真正从磁盘中清除。
 [root@test-os ~]# lsof |grep deleted 
 rsyslogd   1250      root    1w      REG                8,3 4888889358     140789 /var/log/messages (deleted)
 
 [root@test-os ~]# #lsof 显示出系统中被打开的文件 
 [root@test-os ~]# #第一列 软件/服务的名称
 [root@test-os ~]# #第八列 文件的大小 
 [root@test-os ~]# #第10列 文件的名字
 [root@test-os ~]# #第11列 标记（硬链接数为0 进程调用数不为零 就会有 delete)
 
 ####重启对应的服务 释放磁盘空间 
 [root@test-os ~]# /etc/init.d/rsyslog restart 
 --------------------- 
 [root@test-os ~]# df -h
 Filesystem      Size  Used Avail Use% Mounted on
 /dev/sda3       8.8G  1.5G  6.9G  18% /
 tmpfs           931M     0  931M   0% /dev/shm
 /dev/sda1       190M   40M  141M  22% /boot
 /tmp/1m        1003K   19K  933K   2% /app/logs
 --------------------- 
 # df -ih 节点磁盘空间
 Filesystem Inodes IUsed IFree IUse% Mounted on
 /dev/vda1 1.9M 299K 1.6M 17% /
 udev  123K 299 123K 1% /dev
 tmpfs  126K 249 125K 1% /run
 tmpfs  126K 4 126K 1% /run/lock
 tmpfs  126K 2 126K 1% /run/shm
 可以看到，inode 区域只被占用了一小部分，还有大量的空间未使用，所以也不是 inode 区域被占满的问题。
 查看哪个目录占用过大：
 
 cd /;du  -sh ./* |sort -nr|more
 发现 /var/spool/postfix/maildrop 这个目录占用了12G 多的空间
 删除这个目录下的内容(通过管道的方式删除,避免参数过长导致无法执行)：
 
 ls | xargs rm -f
 lsof test.log
 这命令只能在文件被写入的时候才能显示内容，最后虽然得到了个进程号，但是因为写完进程就关闭了，所以还是查不到
 
    for i in /home/*; do echo $i; find $i |wc -l; done
 
    通过该命令可以查看每个用户home下inode的占用情况。如果某个目录下的inode很大，那就是问题所在了。
 
```
### git push 报错 fatal: HttpRequestException encountered.
```javascript
Github 禁用了TLS v1.0 and v1.1，必须更新Windows的git凭证管理器

https://github.com/Microsoft/Git-Credential-Manager-for-Windows/releases/tag/v1.18.2

下载并安装后即可解决
 
```
### pip install 失败
```javascript

 pip install myqr
Collecting myqr
  Retrying (Retry(total=4, connect=None, read=None, redirect=None, status=None)) after connection broken by 'ProxyError('Cannot connect to proxy.', NewConnectionError('<pip._vendor.urllib3.connection.VerifiedHTTPSConnection object at 0x03B22330>: Failed to establish a new connection: [WinError 10061] 由于目
标计算机积极拒绝，无法连接。'))': /simple/myqr/
打开Internet选项，选择连接–局域网设置  全部取消勾选，问题解决
https://blog.csdn.net/u010784236/article/details/51820284
```
### awk
```javascript
$ echo 'this is a test' | awk '{print $0}'
$ awk -F ':' '{ print $1 }' demo.txt
$(NF-1) 代表倒数第二个字段。
 
$ awk -F ':' '{print $1, $(NF-1)}' demo.txt
 NR 表示当前处理的是第几行。
 
$ awk -F ':' '{print NR ") " $1}' demo.txt
awk -F ':' '{ print toupper($1) }' demo.txt
awk -F ':' '/usr/ {print $1}' demo.txt #awk ‘条件 动作’ 文件名  print 命令前面是一个正则表达式，只输出包含 usr 的行
awk -F ':' 'NR % 2 == 1 {print $1}' demo.txt 输出奇数行
awk -F ':' '{if ($1 > "m") print $1}' demo.txt
$ awk -F ':' '{if ($1 > "m") print $1; else print "---"}' demo.txt
```
### 批量删除 .DS_Store 文件
`find /home/path -name ".DS_Store" -type f -delete`

### 查看硬盘及内存空间
```javascript
 
# 查看内存及swap大小
free -m
# 查看当前文件夹下所有文件大小（包括子文件夹）
du -sh
# 查看指定文件夹下所有文件大小
du -h /tmp
# 查看tmp目录(包含子目录)的大小
du -ah /tmp

# 查看指定文件夹大小
du -hs ftp
# 查看磁盘剩余空间
df -hl
# 查看每个根路径的分区大小
df -h
# 返回该目录的大小
du -sh [目录名]
# 返回该文件夹总M数
du -sm [文件夹]
```
### 统计文件或目录的个数
```javascript
# 统计文件夹下文件的个数
ls -l | grep '^-' | wc -l
# 统计文件夹下目录的个数
ls -l | grep '^d' | wc -l
# 统计文件夹下文件的个数(包括子文件夹里的)
ls -lR | grep '^-' | wc -l
# 统计文件夹下目录的个数(包括子文件夹里的)
ls -lR | grep '^d' | wc -l
# 统计/var/www目录下的所有py文件
ls -l /var/www | grep py | wc -l
# 统计/var/www目录下(包括子文件夹里的)的所有py文件
ls -lR /var/www | grep py | wc -l
```
### 查看CPU和内存占用
```javascript
# CPU占用最多的前10个进程： 
ps auxw|head -1;ps auxw|sort -rn -k3|head -10 
# 内存消耗最多的前10个进程 
ps auxw|head -1;ps auxw|sort -rn -k4|head -10 
# 虚拟内存使用最多的前10个进程 
ps auxw|head -1;ps auxw|sort -rn -k5|head -10
 按内存占用对进程排序
ps auxw --sort=rss
# 按CPU占用对进程排序
ps auxw --sort=%cpu

查看所有运行的进程
ps -A
```
### curl
```javascript
强制指定本地端口
curl --local-port 51 http://web.example.com

# 查看连接的详细信息
# --trace-time 跟踪/详细输出时，添加时间戳
curl -Sv --trace-time http://web.example.com

```
### 连接你服务器 top10 用户端的 IP 地址
```javascript
netstat -nat | awk '{print $5}' | awk -F ':' '{print $1}' | sort | uniq -c | sort -rn | head -n 10


```
### 最常用的10个命令
```javascript
cat .bash_history | sort | uniq -c | sort -rn | head -n 10 (or cat .zhistory | sort | uniq -c | sort -rn | head -n 10


```
### alias
```javascript
alias nis="npm install --save "
alias svim='sudo vim'
alias mkcd='foo(){ mkdir -p "$1"; cd "$1" }; foo '
alias install='sudo apt get install'
alias update='sudo apt-get update; sudo apt-get upgrade'
alias ..="cd .."
alias ...="cd ..; cd .."
alias www='python -m SimpleHTTPServer 8000'
alias sock5='ssh -D 8080 -q -C -N -f user@your.server'
```
### wget
```javascript
wget http://www.openss7.org/repos/tarballs/strx25-0.9.2.1.ta
$ wget -b http://www.openss7.org/repos/tarballs/strx25-0.9.2.1.tar.bz2
Continuing in background, pid 1984.
Output will be written to `wget-log'.
wget -i download-file-list.txt


```
### 防火墙
```javascript
sudo firewall-cmd --zone=public --add-port=60001/udp --permanent
sudo firewall-cmd --reload
#之后检查新的防火墙规则
firewall-cmd --list-all

//临时关闭防火墙,重启后会重新自动打开
systemctl restart firewalld
//检查防火墙状态
firewall-cmd --state
firewall-cmd --list-all
//Disable firewall
systemctl disable firewalld
systemctl stop firewalld
systemctl status firewalld
//Enable firewall
systemctl enable firewalld
systemctl start firewalld
systemctl status firewalld
```
### 显示 Path 环境变量
```javascript
 $ echo $PATH | tr : \\n
/data2/node-v6.11.0-linux-x64//bin
/usr/bin
/bin
/usr/sbin
/sbin
/usr/local/bin
/usr/local/sbin
/data0/opt/python27/bin

```
### 判断字符串包含
```javascript
if [[ $tar =~ tar.gz ]];then echo "包含";fi
```
### 快速修改后缀名字
```javascript
ll
CentOS-base.repo.repo.bak
epel.repo.repo.bak

ls *.bak|awk -F. '{print $1}'|xargs -t -i mv {}.repo.repo.bak {}.repo
```
### 数值排序
```javascript
sort -t ":" -k 3 -n /etc/passwd
```
### 清理httpd服务日志超过3天的内容
```javascript
0 5 * * * /usr/bin/find /var/log/httpd/ -type f -mtime +3 -exec rm -rf {} \;
```
### awk获取json
```javascript
{"name": "michael", "sex": "male", "pkg_url": "www.github.com", "number": "888"}

pkg_url=$(echo $env | awk -F "pkg_url\": \"" '{print $2}' | awk -F "\"," '{print $1}')
echo $pkg_url
www.github.com
```
### cpu
```javascript
查看 CPU 的型号

 cat /proc/cpuinfo | grep 'model name' | sort | uniq
查看 CPU 颗数

实际 Server 中插槽上的 CPU 个数，物理 cpu 数量，可以数不重复的 physical id 个数。

cat /proc/cpuinfo | grep 'physical id' | sort | uniq | wc -l
查看 CPU 核数

一颗 CPU 上面能处理数据的芯片组的数量。

cat /proc/cpuinfo |grep "cores"|uniq|awk '{print $4}'
top 命令查询出来的就是逻辑 CPU 的数量。

cat /proc/cpuinfo |grep "processor"|wc -l
https://learnku.com/articles/32681
```
### 查看系统负载
```javascript
$ uptime\
16:33:56 up 69 days,  5:10,  1 user,  load average: 0.14, 0.24, 0.29
以上信息的解析如下：

16:33:56 : 当前时间
up 69 days, 5:10 : 系统运行了 69 天 5 小时 10 分
1 user : 当前有 1 个用户登录了系统 load average: 0.14, 0.24, 0.29 : 系统在过去 1 分钟内，5 分钟内，15 分钟内的平均负载
load average: 0.14, 0.24, 0.29 : 系统在过去 1 分钟内，5 分钟内，15 分钟内的平均负载
平均负载解析

查看逻辑 CPU 核心数：

$ grep 'model name' /proc/cpuinfo | wc -l\
1\
运行结果表示，有 1 个逻辑 CPU 核心。以 1 个 CPU 核心为例，假设 CPU 每分钟最多处理 100 个进程 –

load=0，没有进程需要 CPU
load=0.5，CPU 处理了 50 个进程
load=1, CPU 处理了 100 个进程，这时 CPU 已被占满，但系统还是能顺畅运作的
load=1.5, CPU 处理了 100 个进程，还有 50 个进程正在排除等着 CPU 处理，这时，CPU 已经超负荷工作了
为了系统顺畅运行，load 值最好不要超过 1.0，这样就没有进程需要等待了，所有进程都能第一时间得到处理。\
很显然，1.0 是一个关键值，超过这个值，系统就不在最佳状态了。 一般 0.7 是一个比较理想的值。\
另外，load 值的健康状态还跟系统 CPU 核心数相关，如果 CPU 核心数为 2，那么 load 值健康值应该为 2，以此类推。 \
评价系统的负载一般采用 15 分钟内的那个平均负载值。

二、w 命令

$ w\
 17:47:40 up 69 days,  6:24,  1 user,  load average: 0.46, 0.26, 0.25\
USER     TTY      FROM              LOGIN@   IDLE   JCPU   PCPU WHAT\
lvinkim  pts/0    14.18.144.2      15:55    0.00s  0.02s  0.00s w
第 1 行：与 uptime 一相同。 \
第 2 行以下，当前登录用户的列表。

三、top 命令

$ top\
top - 17:51:23 up 69 days,  6:28,  1 user,  load average: 0.31, 0.30, 0.26\
Tasks:  99 total,   1 running,  98 sleeping,   0 stopped,   0 zombie\
Cpu(s):  2.3%us,  0.2%sy,  0.0%ni, 97.4%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st\
Mem:   1922244k total,  1737480k used,   184764k free,   208576k buffers\
Swap:        0k total,        0k used,        0k free,   466732k cached\
\
  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                                                                \
    1 root      20   0 19232 1004  708 S  0.0  0.1   0:01.17 init                                                                    \
    2 root      20   0     0    0    0 S  0.0  0.0   0:00.01 kthreadd                                                                \
...
第 1 行：与 uptime 一相同。

第 2 行：进程数信息。

Tasks: 99 total : 总共有 99 个进程
1 running : 1 个进程正在占用 CPU
98 sleeping : 98 个睡眠进程
0 stopped : 0 个停止的进程
0 zombie : 0 个僵尸进程
第 3 行 : CPU 使用率

us (user): 非 nice 用户进程占用 CPU 的比率
sy (system): 内核、内核进程占用 CPU 的比率
ni (nice): 用户进程空间内改变过优先级的进程占用 CPU 比率
id (idle): CPU 空闲比率，如果系统缓慢而这个值很高，说明系统慢的原因不是 CPU 负载高
wa (iowait): CPU 等待执行 I/O 操作的时间比率，该指标可以用来排查磁盘 I/O 的问题，通常结合 wa 和 id 判断
hi (Hardware IRQ): CPU 处理硬件中断所占时间的比率
si (Software Interrupts): CPU 处理软件中断所占时间的比率
st (steal): 流逝的时间，虚拟机中的其他任务所占 CPU 时间的比率
需要注意的一些情形：

用户进程 us 占比高，I/O 操作 wa 低：说明系统缓慢的原因在于进程占用大量 CPU，通常还会伴有教低的空闲比率 id，说明 CPU 空转时间很少。
I/O 操作 wa 低，空闲比率 id 高：可以排除 CPU 资源瓶颈的可能。
I/O 操作 wa 高：说明 I/O 占用了大量的 CPU 时间，需要检查交换空间的使用，交换空间位于磁盘上，性能远低于内存，当内存耗尽开始使用交换空间时，将会给性能带来严重影响，所以对于性能要求较高的服务器，一般建议关闭交换空间。另一方面，如果内存充足，但 wa 很高，说明需要检查哪个进程占用了大量的 I/O 资源。
更多负载情形，可在实际中灵活判断。

四、iostat 命令

iostat 命令可以查看系统分区的 IO 使用情况

$ iostat \
Linux 2.6.32-573.22.1.el6.x86_64 (sgs02)   01/20/2017     _x86_64_   (1 CPU)\
\
avg-cpu:  %user   %nice %system %iowait  %steal   %idle\
           2.29    0.00    0.25    0.04    0.00   97.41\
\
Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn\
vda               1.15         3.48        21.88   21016084  131997520
一些值得注意的 IO 指标 :

Device : 磁盘名称
tps : 每秒 I/O 传输请求量
Blk_read/s : 每秒读取多少块，查看块大小可参考命令 tune2fs
Blk_wrtn/s : 每秒写取多少块
Blk_read : 一共读了多少块
–Blk_wrtn : 一共写了多少块
五、iotop 命令

iotop 命令类似于 top 命令，但是显示的是各个进程的 I/O 情况，对于定位 I/O 操作较重的进程有比较大的作用。\

# iotop\
Total DISK READ: 0.00 B/s | Total DISK WRITE: 774.52 K/s\
  TID  PRIO  USER     DISK READ  DISK WRITE  SWAPIN     IO>    COMMAND                                                                \
  272 be/3 root        0.00 B/s    0.00 B/s  0.00 %  4.86 % [jbd2/vda1-8]\
 9072 be/4 mysql       0.00 B/s  268.71 K/s  0.00 %  0.00 % mysqld\
 5058 be/4 lvinkim     0.00 B/s    3.95 K/s  0.00 %  0.00 % php-fpm: pool www\
    1 be/4 root        0.00 B/s    0.00 B/s  0.00 %  0.00 % init
可以看到不同任务的读写强度。

六、sysstat 工具

很多时候当检测到或者知道历史的高负载状况时，可能需要回放历史监控数据，这时 sar 命令就派上用场了，sar 命令同样来自 sysstat 工具包，可以记录系统的 CPU 负载、I/O 状况和内存使用记录，便于历史数据的回放。

sysstat 的配置文件在 /etc/sysconfig/sysstat 文件，历史日志的存放位置为 /var/log/sa\
统计信息都是每 10 分钟记录一次，每天的 23:59 会分割统计文件，这些操作的频率都在 /etc/cron.d/sysstat 文件配置。\

七、sar 命令

使用 sar 命令查看当天 CPU 使用：

$ sar\
Linux 2.6.32-431.23.3.el6.x86_64 (szs01)   01/20/2017     _x86_64_   (1 CPU)\
\
10:50:01 AM     CPU     %user     %nice   %system   %iowait    %steal     %idle\
11:00:01 AM     all      0.45      0.00      0.22      0.40      0.00     98.93\
Average:        all      0.45      0.00      0.22      0.40      0.00     98.93
使用 sar 命令查看当天内存使用：

$ sar -r\
Linux 2.6.32-431.23.3.el6.x86_64 (szs01)   01/20/2017     _x86_64_   (1 CPU)\
\
10:50:01 AM kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit\
11:00:01 AM     41292    459180     91.75     44072    164620    822392    164.32\
Average:        41292    459180     91.75     44072    164620    822392    164.32
使用 sar 命令查看当天 IO 统计记录：

$ sar -b\
Linux 2.6.32-431.23.3.el6.x86_64 (szs01)   01/20/2017     _x86_64_   (1 CPU)\
\
10:50:01 AM       tps      rtps      wtps   bread/s   bwrtn/s\
11:00:01 AM      3.31      2.14      1.17     37.18     16.84\
Average:         3.31      2.14      1.17     37.18     16.84
https://learnku.com/articles/31718
```
### 秘钥登录 
```javascript
ssh-keygen -t rsa -C 'youxiang@aliyun.com'
scp -P <端口号> ~/.ssh/id_rsa.pub <用户名>@<ip地址>:/home/id_rsa.pub
cat /home/id_rsa.pub >> ~/.ssh/authorized_keys

如果在 家目录 没有 .ssh 目录或 authorized_keys 文件，可以创建一下，并授予 authorized_keys 文件 600 权限
ssh root@114.11.11.111
vi ~/.ssh/config

Host            jd            #自定义别名
HostName        114.11.11.110         #替换为你的ssh服务器ip或domain
Port            22             #ssh服务器端口，默认为22
User            root             #ssh服务器用户名
IdentityFile    ~/.ssh/id_rsa    #第一个步骤生成的公钥文件对应的私钥文件
此时就可以使用 ssh jd 进行登录
cd /etc/ssh/

修改 SSH 的配置文件 vi sshd_config

RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
#AuthorizedKeysCommand none
#AuthorizedKeysCommandRunAs nobody

#默认PasswordAuthentication 为yes,即允许密码登录，改为no后，禁止密码登录
PasswordAuthentication no 
重启 ssh 服务

systemctl restart sshd.service
https://learnku.com/articles/30438
```
### 生成二维码
```javascript
第一种方法： qrencode
#安装，记得加epel yum源
[root@localhost ~]# yum install libpng libpng-devel qrencode -y


#格式
Usage: qrencode [OPTION]... [STRING]
OPTIONS：
-o：输出的二维码文件名。如test.png。需要以.png结尾。-表示输出到控制台。
-s：指定图片大小。默认为3个像素。
-t：指定产生的图片类型。默认为PNG。可以是PNG/ANSI/ANSI256/ASCIIi/UTF8等。如果需要输出到控制台，可以用ANSI、ANSI256等
STRING：可以是text、url等


#使用
注：输出的二维码图片大小取决于，内容的多少，且不能换行

# 将“http://jinchuang.org”网址转换为二维码并保存在qrcode.png图片中
[root@localhost ~]# qrencode -o qrcode.png "http://jinchuang.org"

# 在终端下无法查看png图片，所以可以使用ANSI生成
[root@localhost ~]# echo "your input words" | qrencode -o - -t ANSI

# 也可以将需要转换的文字保存为xx.txt文件，之后再转换
[root@localhost ~]# cat xx.txt | qrencode -o - -t ANSI

第二种方法：
[root@localhost ~]# printf "http://jinchuang.org" | curl -F-=\<- qrenco.de 

```
### es Kibana
```javascript

[root@localhost ~]# cd /source
[root@localhost source]# wget https://artifacts.elastic.co/downloads/kibana/kibana-5.6.0-linux-x86_64.tar.gz
[root@localhost source]# tar xf kibana-5.6.0-linux-x86_64.tar.gz  -C /elk/
[root@localhost source]# cd /elk/
[root@localhost elk]# mv kibana-5.6.0-linux-x86_64/ kibana

#修改配置文件
[root@localhost elk]# vim kibana/config/kibana.yml
server.port: 5601
server.host: "0.0.0.0"
elasticsearch.url: "http://127.0.0.1:9200"

#启动
[root@localhost elk]# /elk/kibana/bin/kibana &
[1] 31948
  log   [05:53:04.064] [info][status][plugin:kibana@5.6.0] Status changed from uninitialized to green - Ready
  log   [05:53:04.149] [info][status][plugin:elasticsearch@5.6.0] Status changed from uninitialized to yellow - Waiting for Elasticsearch
  log   [05:53:04.190] [info][status][plugin:console@5.6.0] Status changed from uninitialized to green - Ready
  log   [05:53:04.235] [info][status][plugin:metrics@5.6.0] Status changed from uninitialized to green - Ready
  log   [05:53:04.435] [info][status][plugin:timelion@5.6.0] Status changed from uninitialized to green - Ready
  log   [05:53:04.447] [info][status][plugin:elasticsearch@5.6.0] Status changed from yellow to green - Kibana index ready
  log   [05:53:04.449] [info][listening] Server running at http://0.0.0.0:5601
  log   [05:53:04.452] [info][status][ui settings] Status changed from uninitialized to green - Ready

#查看进程和端口
[root@localhost elk]# ps -ef|grep kibana
root     31948 31038 12 13:52 pts/3    00:00:04 /elk/kibana/bin/../node/bin/node --no-warnings /elk/kibana/bin/../src/cli

[root@localhost elk]# netstat -lntp 
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:9100            0.0.0.0:*               LISTEN      28361/grunt         
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      14146/sshd      

https://me.jinchuang.org/archives/319.html
```
### 随机数生成
```javascript
(1)  echo $(($RANDOM))          通过系统环境变量

(2)  echo $RANDOM | md5sum|cut -c 1-8

(3)  openssl rand -base64 65         openssl产生随机数

(4)  date +%s%N          通过时间获取随机数

(5)  head /dev/urandom |cksum            设备随机数

(6)  cat /proc/sys/kernel/random/uuid           uuid随机数

(7)  mkpasswd -l 12 -d 5             expect随机数，需要安装expect

```
### Elasticsearch 在 docker 和 CentOS 下的安装教程
```javascript
sudo docker pull docker.elastic.co/elasticsearch/elasticsearch:7.3.1

sudo docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.3.1

sudo docker run -itd -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.3.1
https://learnku.com/articles/33404
```
### wget 命令提示 “use ‘--no-check-certificate
```javascript
php -r "print_r(openssl_get_cert_locations());"
可以看到证书的默认位置在 /etc/pki/tls/cert.pem，这是在安装 Apache 时自动生成的证书文件。如果你的目录里没有证书文件，使用下面的命令下载一个：

$wget -c https://curl.haxx.se/ca/cacert.pem  /tmp --no-check-certificate
因为我系统上本来就有一个证书文件了，为了不影响原来的，我将新的证书文件下载到 /tmp 目录中。然后设置以下环境变量，使证书只在当前会话生效：

export SSL_CERT_FILE=/tmp/cacert.pem
https://learnku.com/articles/33549

```
### docker
```javascript
如果我们想在一台电脑上搭建各种开发环境：lnmp 环境，java 环境，nodejs，redis 集群，python 开发环境。显然，这些多的开发环境，如果集中在一台机器上构建，会让系统显得复杂，可能还会出现各种版本或依赖之间的不兼容。如果能将这些开发环境都独立开来，各个环境互相独立隔离，但又能互相通讯交互，相当于每个环境都是一个容器，这些容器可以独立提供服务，也能通信交互。

docker 就是这样的容器技术。

用官方术语描述 docker: Docker 是基于 Go 语言实现的开源容器项目，有效地将由单个操作系统管理的资源划分到孤立的组中，以更好地在孤立的组之间平衡有冲突的资源使用需求。

通俗地理解：docker 能让你在一台物理机上构建出很多个轻量极的开发环境。
https://learnku.com/articles/33670#reply108346
```
### xargs 命令
```javascript
echo命令就不接受管道传参。

$ echo "hello world" | echo
xargs命令的作用，是将标准输入转为命令行参数
$ echo "hello world" | xargs echo
hello world
$ echo "one two three" | xargs mkdir

$ xargs
# 等同于
$ xargs echo
-d参数可以更改分隔符。

$ echo -e "a\tb\tc" | xargs -d "\t" echo
a b c

$ echo 'one two three' | xargs -p touch
touch one two three ?...
上面的命令执行以后，会打印出最终要执行的命令，让用户确认。用户输入y以后（大小写皆可），才会真正执行。
xargs特别适合find命令。有些命令（比如rm）一旦参数过多会报错"参数列表过长"，而无法执行，改用xargs就没有这个问题，因为它对每个参数执行一次命令。

$ find . -name "*.txt" | xargs grep "abc"
上面命令找出所有 TXT 文件以后，对每个文件搜索一次是否包含字符串abc
http://www.ruanyifeng.com/blog/2019/08/xargs-tutorial.html
```
### 禁用 eval 函数
```javascript
在 /etc/bashrc 里加入
alias eval = 'echo'

```
###  find 模拟 tree 
```javascript
$ find . -print | sed -e ``'s;[^/]*/;|____;g;s;____|; |;g'


```
[初识 Shell](https://learnku.com/articles/33469)

[html 转 pdf 命令行工具 wkhtmltopdf ](http://einverne.github.io/post/2019/01/html-to-pdf.html)

[Centos7 搭建 Nextcloud个人网盘](https://me.jinchuang.org/archives/301.html)

[三十分钟学会sed](https://github.com/mylxsw/growing-up/blob/master/doc/%E4%B8%89%E5%8D%81%E5%88%86%E9%92%9F%E5%AD%A6%E4%BC%9ASED.md)

[Linux工具快速教程](https://linuxtools-rst.readthedocs.io/zh_CN/latest/index.html)

[看例子学sed](http://qinghua.github.io/sed/)

[工作中常用的 Linux 命令](https://michael728.github.io/2018/07/05/linux-useful-commands-in-work/)

[工作中常用的 Shell 命令及技巧](https://michael728.github.io/2019/04/14/linux-useful-shell-commands-in-work/)

[PHPer 必知必会的 Linux 命令](https://linux.hellocode.name/user-group.html)

[Linux工具快速教程](https://linuxtools-rst.readthedocs.io/zh_CN/latest/index.html#)

[你可能不知道的shell技巧](https://segmentfault.com/a/1190000016648034)

[wget的15个震撼的例子](https://neuqzxy.github.io/2017/07/08/wget%E7%9A%8415%E4%B8%AA%E9%9C%87%E6%92%BC%E7%9A%84%E4%BE%8B%E5%AD%90/)

[Linux 踩坑记](https://www.restran.net/2015/10/19/linux-practice-notes/)

[PHPer 必知必会的 Linux 命令](https://github.com/Nick233333/phper-linux-gitbook)

[命令行的艺术](https://github.com/jlevy/the-art-of-command-line)

[Nginx 学习笔记时序图](https://www.njphper.com/posts/680ebdf2.html)

[Windows 10 安装体验](https://learnku.com/articles/28500)

[打造高效的工作环境 – SHELL 篇https://github.com/coolshellx/articles](https://coolshell.cn/articles/19219.html)


[工作中常用的 Linux 命令](https://michael728.github.io/2018/07/05/linux-useful-commands-in-work/)