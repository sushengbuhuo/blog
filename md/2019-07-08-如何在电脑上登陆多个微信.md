---
title: 如何在电脑上登陆多个微信
date: 2019-07-08 20:19:46
tags:
- 公众号
---

电脑上习惯登录好几个QQ，像这样的。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-ab412be6302eedb1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

但pc端的微信软件默认只能登录一个微信，如果有多个微信怎么办呢？
### 微信网页版
微信除了pc端的软件还有个微信网页版，地址是 [https://wx.qq.com/](https://wx.qq.com/) ，打开几个浏览器就能登录几个微信，一个浏览器当然也可以，开启隐身模式，见之前的文章[那些你可能不知道的浏览器奇技淫巧](https://mp.weixin.qq.com/s/-cSjrvkibYGp5Fx8gCTFuw) 。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-f68c8590031b7273.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
登录后
![image.png](https://upload-images.jianshu.io/upload_images/17817191-75676b4fc2039cd9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
还可以查看公众号文章。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-0cabf4ea40a7398e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

如果你觉得网页版样式不好看，可以安装个Chrome扩展把网页微信伪装成云笔记 [https://github.com/YGYOOO/WeChat-Shelter](https://github.com/YGYOOO/WeChat-Shelter)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-1167b3a7ad4d1103.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
使用后的效果
![image.png](https://upload-images.jianshu.io/upload_images/17817191-8415fb5544afae16.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 命令行
微信网页版虽然可以用，但是切换浏览器麻烦，而且不保存聊天记录，关闭后再登录就没有记录了。所以还是用pc端的软件比较好，它能保存所有聊天记录，图片和视频。

那如何能在pc端登录多个微信呢？

首先右击桌面的微信快捷方式，找到微信的安装目录，比如我的。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-70408f5695555f1f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

安装路径为 D:\wechat\WeChat.exe，将路径D:\wechat添加到环境变量。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-94298c8075d7f07b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

打开命令行执行`wechat & wechat & wechat`就可以打开3个微信了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-d91b1e9cf8e0c8ce.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 批处理
如果觉得这样麻烦新建个批处理文件 `wechat.bat`
```js
start   "D:\wechat\WeChat.exe"
start   "D:\wechat\WeChat.exe"
```
先关闭登录的微信，然后双击wechat.bat就可以登录2个微信了。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-acd98c7823b55383.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
登录后的效果。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-fa204cd566605225.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### win10应用商店
如果你用的win10，还可以在应用商店安装`微信 For Windows` 地址 [https://www.microsoft.com/zh-cn/p/%e5%be%ae%e4%bf%a1-for-windows/9nblggh4slx7?activetab=pivot:overviewtab](https://www.microsoft.com/zh-cn/p/%e5%be%ae%e4%bf%a1-for-windows/9nblggh4slx7?activetab=pivot:overviewtab)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-c9379dd28a14f5c7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这样有了2个软件就可以登录2个微信了。

除了上面这些方法还有第三方软件[https://github.com/anhkgg/SuperWeChatPC](https://github.com/anhkgg/SuperWeChatPC) 下载地址 https://raw.githubusercontent.com/anhkgg/SuperWeChatPC/master/bin/v1.1.3.zip
这个支持登录多个微信，还能消息防撤回，语音备份等。
mac系统推荐[https://github.com/Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS)
下载源码后sudo make install就好了。
推荐阅读：

[那些你可能不知道的浏览器奇技淫巧](https://mp.weixin.qq.com/s/-cSjrvkibYGp5Fx8gCTFuw)

[那些你可能不知道的微信奇技淫巧](https://mp.weixin.qq.com/s/eGDO0Y8el_dsEyriCoAgog)

[那些你可能不知道的微博奇技淫巧](https://mp.weixin.qq.com/s/j7VhoZXmUTnOWC5C_B8jlQ)

[那些你可能不知道的网易云音乐奇技淫巧](https://mp.weixin.qq.com/s/LtI2piwAIDXA590NEsXvuw)

 [那些你可能不知道的搜索奇技淫巧](https://mp.weixin.qq.com/s?__biz=MzIyMjg2ODExMA==&mid=2247483979&idx=1&sn=0735daa1d805b66d346ed0e8e60a841f&scene=21#wechat_redirect)

[那些你可能不知道的视频下载奇技淫巧](https://mp.weixin.qq.com/s?__biz=MzIyMjg2ODExMA==&mid=2247483983&idx=1&sn=f0e1d9a8e22caf609d6c21431a530186&chksm=e827a5aedf502cb8b72f2036054753fcfd9c20c28b9fbccdeae619a254a80e1024f18ba06523&token=457023358&lang=zh_CN#rd)

[那些你可能不知道的免费观看 VIP 视频奇技淫巧](https://mp.weixin.qq.com/s/R3x-xZwqLIVwPjlgikDQ9A)

[那些你可能不知道的知乎奇技淫巧](https://mp.weixin.qq.com/s/sqRgMh4rxFBt5YxNtaa6dw)
### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


