---
title: 10%+10% 不等于 0.2？
date: 2019-09-16 20:22:14
tags:
- 公众号
---

看到这个标题，你可能会觉得震惊，怎么会不是0.2呢?

最近在知乎上看到这个有意思的问题[https://www.zhihu.com/question/343963978](https://www.zhihu.com/question/343963978)
，为什么手机上的计算器计算 10%+10% 都错了，结果不是0.2，而是0.11。

第一感觉是不可思议，赶紧拿出我的华为手机（EMUI版本为9.1）测试了下，果然是0.11！
![image.png](https://upload-images.jianshu.io/upload_images/17817191-73c08239267c80b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


看看谷歌的计算器，0.2没错。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-8edcae05712bf3fe.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
但是搜索`计算器` 再计算的结果却是0.11，很奇怪。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-863f467eb61f366e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

然后在在线自动问答系统搜索引擎wolframalpha上看看[https://www.wolframalpha.com/input/?i=10%25%2B10%25](https://www.wolframalpha.com/input/?i=10%25%2B10%25)

![image.png](https://upload-images.jianshu.io/upload_images/17817191-dd8788861fc35820.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
它给出了3种结果，有0.2和0.11 。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-5f4129ed1fdeb45f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

ps：`wolframalpha`这个网站计算数学很不错，比如黄金分割比0.618
![image.png](https://upload-images.jianshu.io/upload_images/17817191-1607024d1915ae17.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

再看看国家统计局的解释
[http://www.stats.gov.cn/tjzs/tjcd/200205/t20020523_25320.html](http://www.stats.gov.cn/tjzs/tjcd/200205/t20020523_25320.html)
[https://cn.v2ex.com/t/597205?p=1](https://cn.v2ex.com/t/597205?p=1)
![image.png](https://upload-images.jianshu.io/upload_images/17817191-0099f8eef073a704.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

搜索了下苹果和小米等手机都是如此。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-90020f8426177013.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

然而魅族手机却是个例外。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-5265224ae0f486c9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


魅族副总裁在微博上的解释是这样的 [https://weibo.com/1444241363/I554TgDEs?](https://weibo.com/1444241363/I554TgDEs?)
> 本质上并不是对或错的问题，我们只是考虑的更多，增加了人性化的算法判断。10%+10%=0.11 这个是国外的使用逻辑，而国内的算数逻辑是 10%+10%=0.2，所以我们针对不同地区的固件做了不同的算法判断。 

>  算小费等特殊场景，很多国家餐厅吃饭给10-20%的小费，这时100+10%相当于100+100*10%，相当方便的一种人性化设计。

![image.png](https://upload-images.jianshu.io/upload_images/17817191-53fad4198a0e3e79.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

所以10%+10%是按照10%+（10% x 10%）这个逻辑得出0.11的结果，10+10%就是11。

只能说国内和国外使用习惯不同，中国人没有付小费的习惯，而且我们一般是用0.1+0.1算的，这应该算计算器的feature，不算bug。
![image.png](https://upload-images.jianshu.io/upload_images/17817191-8e845b47fe8eb3a7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


 看看你的手机计算器结果是0.2还是0.11？


![免费星球](https://upload-images.jianshu.io/upload_images/17817191-393b26173c148690.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 公众号：苏生不惑
 ![扫描二维码关注](https://upload-images.jianshu.io/upload_images/17817191-6e0079f95d4c0338.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)